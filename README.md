
| [Website](http://links.otrenav.com/website) | [Twitter](http://links.otrenav.com/twitter) | [LinkedIn](http://links.otrenav.com/linkedin)  | [GitHub](http://links.otrenav.com/github) | [GitLab](http://links.otrenav.com/gitlab) | [CodeMentor](http://links.otrenav.com/codementor) |

---

# App integration tools class

- Omar Trejo
- May, 2016

This is the code for the final project for a class that I took at ITAM for
concurrent systems. The code is for a basic online shop. It was created using
Java and OpenESB, a Business Process Execution Language (BPEL) engine for the
Enterprise Service Bus (ESB) architecture. This repository is for pedagogic
purposes about ESB and BPEL. Other than that it's not very useful.

---

> "The best ideas are common property."
>
> —Seneca
